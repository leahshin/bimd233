//constructor function for company objects
function company(name, mc, sales, profit, employees) {
  this.name = name;
  this.marketCap = mc;
  this.sales = sales;
  this.profit = profit;
  this.employees = employees;
}

//creating an array of companies
var list = [];
list[0] = new company("Microsoft", "$381.7 B", "$86.8 B", "$22.1 B", "128,000");
list[1] = new company("Symetra Financial", "$2.7 B", "$2.2 B", "$254.4 M", "1,400");
list[2] = new company("Micron Technology", "$37.6 B", "$16.4 B", "$3.0 B", "30,400");
list[3] = new company("F5 Networks", "$9.5 B", "$526.3 M", "$311.2 M", "3,834");
list[4] = new company("Expedia", "$10.8 B", "$5.8 B", "$398.1 M", "18,210");
list[5] = new company("Nautilus", "$476 M", "$274.4 M", "$18.8 M", "340");
list[6] = new company("Heritage Financial", "$531 M", "$137.6 M", "$21 M", "748");
list[7] = new company("Cascade Microtech", "$239 M", "$19.3 M", "$9.9 M", "449");
list[8] = new company("Nike", "$83.1 B", "$27.8 B", "$2.7 B", "56,500");
list[9] = new company("Alaska Air Group", "$7.9 B", "$5.4 B", "$605 M", "13,952");

//grabbing the html element I want to fill
var demoP = document.getElementById("table");

//Function that SHOULD be appending the inner html with the current index of the company array
function myFunction(item, index) {
  var table = document.getElementById("myTable");
  var row = table.insertRow(index + 1);
  console.log(list[index].name);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  cell1.innerHTML = list[index].name;
  cell2.innerHTML = list[index].marketCap;
  cell3.innerHTML = list[index].sales;
  cell4.innerHTML = list[index].profit;
  cell5.innerHTML = list[index].employees;
  
}

$(document).ready(function(){
  var $prev = $('.slideshow__nav_link--is-previous');
  var $next = $('.slideshow__nav_link--is-next');
  var mode = "auto";
  $prev.on({
    click: function(e){
      e.preventDefault();
      mode = "manual";
      showPreviousImage();
    }
  });
  $next.on({
    click: function(e){
      e.preventDefault();
      mode = "manual";
      showNextImage();
      
    }
  });
  
  setInterval(function(){
    if(mode==="auto"){
      showNextImage();
    }
  },3000);
  
  function showNextImage(){
      var $actEl = $('.slideshow__slide--is-active');
      var $nextEl = $actEl.next('.slideshow__slide');
      if($nextEl.length){
        $actEl.removeClass('slideshow__slide--is-active');
        $nextEl.addClass('slideshow__slide--is-active');
      }else{
        $actEl.removeClass('slideshow__slide--is-active');
        $('.slideshow__slide:first-child').addClass('slideshow__slide--is-active');
      }
  }
  
  function showPreviousImage(){
      var $actEl = $('.slideshow__slide--is-active');
      var $prevEl = $actEl.prev('.slideshow__slide');
      if($prevEl.length){
        $actEl.removeClass('slideshow__slide--is-active');
        $prevEl.addClass('slideshow__slide--is-active');
      }else{
        $actEl.removeClass('slideshow__slide--is-active');
        $('slideshow__slide--is-last').addClass('slideshow__slide--is-active');
      }
  }
});