// var w = $('li')[0].width;
$('li').css("id", "uw");
const states = ["idle", "gather", "process"];
var state = states[0];
var words = new Array;
var ndx = 0;

$('ul').on("mouseover", "li", function() {
  console.log('x:' + $(this).text());
  $(this).attr('id', 'uw');
});

$('ul').on("mouseleave", "li", function() {
  $(this).attr('id', 'uw-gold');
});

// reset button click
$('button').on('click', function(e) {
  $('ul li').each( function () {
    console.log("this:"+this);
    $(this).remove();
  });
  $('input').val("");
  words = new Array;
  state = states[0];
});

// keypress


$('input').on('keypress', function(e) {
  var code = e.which;
  var char = String.fromCharCode(code);
  // console.log('key:' + code + '\tstate:' + state);
  switch (state) {

    // idle  
    case "idle":
      if (code !== 20) {
        state = states[1];
        words[ndx] = char;
      }
      break;

      // gather  
    case "gather":
      if (code !== 13) {
        words[ndx] += char;
      } else {
        var str = "<li>" + words[ndx] + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class','list-group-item');
        $('ul li:last').attr('id','uw-gold');
        $(this).val("");
        ndx++;
        words[ndx] = "";
        state = states[0];
      }
      break;

      // process
    case "process":
      if (code !== " ") {
        state = states[1];
        words[ndx] = char;
      }
      break;

    default:
      break;
  }
});