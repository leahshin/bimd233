$('li').css('margin', '10px');
$('li').attr('id', 'uw');

$('#p1 li').click(function() { // .fadeOut
  $(this).fadeOut(1500, function() {
    console.log(".fadeOut complete.");
  });
});

$('#p2 li').click(function() { // .fadeIn
  $(this).fadeOut(1500, function() {
    $(this).fadeIn('3000', function() {
      console.log(".fadeIn complete.");
    });
  });
});

$('#p3 li').click(function() { // .fadeTo
  $(this).fadeOut(1500, function() {
    $(this).attr('id', 'uw-gold');
    $(this).fadeTo("slow", 0.7, function() {
      console.log(".fadeTo complete.");
    });
  });
});

$('#p4 li').click(function() { // .fadeToggle
  $(this).fadeToggle('slow', 'linear', function() {
    console.log(".fadeToggle complete.");
  });
});