window.onload = function() {
    function flight (airline, number, origin, destination, dep_date, dep_time, arrival_date ,arrival_time, arrival_gate) {
      this.airline = airline;
      this.number = number;
      this.origin = origin;
      this.destination = destination;
      this.dep_date = dep_date;
      this.dep_time = dep_time;
      this.arrival_date = arrival_date;
      this.arrival_time = arrival_time;
      this.arrival_gate = arrival_gate;
      this.duration = function(){
          return (((this.arrival_time)/100) - ((this.dep_time)/100))/60;
      }
    };
    
var Alaska = new flight('Alaska', '(AK333)', 'B737', 'KSEA', 'May 5, 2016 ', 180000, 'May 5, 2016 ', 192000, 'C7');    
        var United = new flight('United', '(U2001)', 'A320', 'KORD', 'May 5, 2016 ', 110000, 'May 5, 2016 ', 525000, 'N17');
        var Delta = new flight('Delta', '(DAL)', 'MD90', 'KORD', 'May 5, 2016 ', 091500, 'May 5, 2016 ', 114000, 'S5' );
        var flights = [Alaska, United, Delta];   
    var html = '';
    for(var i = 0; i<3; i++)
    {
                   html += "<tr><td>"+flights[i]['airline']+"</td><td>"+flights[i]['number']+" </td><td> "+flights[i]['origin']+"</td><td>"+flights[i]['destination']+"</td><td>"+flights[i]['dep_date']+flights[i]['dep_time']+"</td><td>"+flights[i]['arrival_date']+flights[i]['arrival_time']+"</td><td>"+flights[i]['arrival_gate']+"</td><td>"+flights[i].duration().toFixed(2)+"hrs"+"</td></tr>";
    };
                document.getElementById('results').innerHTML = html;
}