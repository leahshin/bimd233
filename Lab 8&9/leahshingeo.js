window.onload = function () 
{
    var r1 = 15;
    var r2 = 50;
    var r3 = 26;
    const pi = Math.PI; 
    var area1 = pi * r1 * r1;
    var area2 = pi * r2 * r2;
    var area3 = pi * r3 * r3;
    var circumference1 = 2 * pi * r1; 
    var circumference2 = 2 * pi * r2;
    var circumference3 = 2 * pi * r3; 
    var diameter1 = 2 * r1; 
    var diameter2 = 2 * r2;
    var diameter3 = 2 * r3;
    var geometries1 = [area1, circumference1, diameter1];
    var geometries2 = [area2, circumference2, diameter2];
    var geometries3 = [area3, circumference2, diameter2];
    var html = "";
    html += "<tr><td>"+geometries1[0].toFixed(2)+"</td><td>"+geometries1[1].toFixed(2)+"</td><td>"+geometries1[2].toFixed(2)+"</td></tr>";
    html += "<tr><td>"+geometries2[0].toFixed(2)+"</td><td>"+geometries2[1].toFixed(2)+"</td><td>"+geometries2[2].toFixed(2)+"</td></tr>";
    html += "<tr><td>"+geometries3[0].toFixed(2)+"</td><td>"+geometries3[1].toFixed(2)+"</td><td>"+geometries3[2].toFixed(2)+"</td></tr>";
    document.getElementById("geo").innerHTML = html;
    
}