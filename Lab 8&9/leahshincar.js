window.onload = function() {
    var Cars = [
      ["Ford", "Gran Torino", "1970", "$54,000"],
      ["BMW", "Z3", "2002", "$31,300"],
      ["Audi", "R8", "2015", "$115,900"],
      ["Mitsubishi", "Lancer Evolution", "2009", "$24,000"],
      ["Chevrolet", "Camaro", "2015", "$23,705"]
    ];
    var html = " ";
    html += "<tr><td>" + Cars[0][0] + "</td><td>" + Cars[0][1] + "</td><td>" + Cars[0][2] + "</td><td>" + Cars[0][3] + "</td></tr>";
    html += "<tr><td>" + Cars[1][0] + "</td><td>" + Cars[1][1] + "</td><td>" + Cars[1][2] + "</td><td>" + Cars[1][3] + "</td></tr>";
    html += "<tr><td>" + Cars[2][0] + "</td><td>" + Cars[2][1] + "</td><td>" + Cars[2][2] + "</td><td>" + Cars[2][3] + "</td></tr>";
    html += "<tr><td>" + Cars[3][0] + "</td><td>" + Cars[3][1] + "</td><td>" + Cars[3][2] + "</td><td>" + Cars[3][3] + "</td></tr>";
    html += "<tr><td>" + Cars[4][0] + "</td><td>" + Cars[4][1] + "</td><td>" + Cars[4][2] + "</td><td>" + Cars[4][3] + "</td></tr>";

    document.getElementById("cardata").innerHTML = html;
}