var book1 = {
  title: "Seveneves",
  author: "Neal Stephenson"
};
var book2 = {
  title: "How Not to be Wrong",
  author: "Jordan Ellenberg"
};
var book3 = {
  title: "The Vital Question",
  author: "Nick Lane"
};
var book4 = {
  title: "The Power to Compete",
  author: "Ryoichi Mikitani and Hiroshi Mikitani"
};
var book5 = {
  title: "Sapiens: A Brief History of Humankind",
  author: "Noah Yaval Harai"
};

var books = new Array;
books.push(book1);
books.push(book2);
books.push(book3);
books.push(book4);
books.push(book5);

var img_ref = {
  url: 'https://fortunedotcom.files.wordpress.com/2014/12/h_14495354.jpg?w=1100&quality=85',
  src: 'https://fortunedotcom.files.wordpress.com/2014/12/h_14495354.jpg?w=1100&quality=85',
  alt: 'Bill Gates',
  height: 240, // orig 401 by 4:1
  width: 440 // orig 534 by 4:1
};

var reference = {
    url: 'http://www.usatoday.com/story/money/markets/2016/05/20/bill-gates-read-these-5-books-summer/84675556/',
    src: 'http://usat.ly/20hirO3',
    alt: 'Gates Books',
    text: 'Bill Gates: 5 Books for Summer 2016'
  }

$('#bg img').attr(img_ref);
$('#b5 a').attr('href', reference.src);
$('#b5 a').attr('alt', reference.alt);
$('#b5 a').text(reference.text);

$('ol').addClass("list-group");
$('li').addClass("list-group-item");

var count = 1;

$('li').each(function(i) { //Selects all HTML elements <li> (unorderd lists)
  if (count % 2 != 0) { //Under the condition that the value of count is not equal to zero after being divided by two, then execute... 
    this.id = 'odd'; //Changes .css selector to the values within 'odd'.
  } else { //Otherwise...
    this.id = 'even'; //Change the id attribute to 'even'.
  } //ends if statement
  this.innerHTML = count + '. ' + '<i>' + books[i].title + '</i>' + ' by ' + books[i].author;
  count++;
});